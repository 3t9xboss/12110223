﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class DongGopYKien
    {
        [Key]
        public int ID { set; get; }
        public int AccID { set; get; }
        public virtual Account Account { set; get; }
        public string NoiDung { set; get; }
    }
}