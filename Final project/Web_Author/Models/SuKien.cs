﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class SuKien
    {
        [Key]
        public int SID { set; get; }
        public string Hinh { set; get; }
        public string NoiDung { set; get; }
        [DataType(DataType.Date)]
        public DateTime NgayTao { set; get; }
    }
}