﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class Account
    {
        [Key]
        public int AccID { set; get; }
        [Required(ErrorMessage="Vui lòng nhập thông tin")]
        [Display(Name="Tên tài khoản *")]
        public string UserName { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [DataType(DataType.Password)]
        [Display(Name = "Mật khẩu *")]
        public string Password { set; get; }
        public DateTime DateCreate { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Họ và tên *")]
        public string Name { set; get; }
        //[Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Địa chỉ")]
        public string Address { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Email *")]
        public string Email { set; get; }
        //[Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { set; get; }
        [DataType(DataType.Date)]
        [Display(Name = "Ngày sinh")]
        public DateTime DayOfBird { set; get; }
        [Display(Name = "Cấp độ")]
        public string AccLevel { set; get; }
        [Display(Name = "Bài đăng")]
        public int AccTotalPost { set; get; }
        [Display(Name = "Ảnh")]
        public string Images { set; get; }
        public virtual ICollection<LikeDiemDuLich> Like { set; get; }
        public virtual ICollection<DongGopYKien> DongGopYKien { set; get; }
    }
    public class ChangePassword
    {
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Tên tài khoản *")]
        public string UserName { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        public string Email { set; get; }
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Ngày sinh *")]
        public DateTime DayOfBird { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name = "Mật khẩu mới *")]
        public string NewPassword { set; get; }
        public string Confirm { set; get; }
    }
    public class LuuTrangThai
    {
        public static int ID;
        public static string UserName="";
        public static string Name = "";
    }
}