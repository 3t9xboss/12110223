﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class WebDbContext:DbContext
    {
        public DbSet<Account> Accounts { set; get; }

        public DbSet<Login> Logins { get; set; }

        public DbSet<SuKien> SuKiens { get; set; }

        public DbSet<TinTuc> TinTucs { get; set; }

        public DbSet<DiemDuLich> DiemDuLiches { get; set; }

        public DbSet<CmtDiemDuLich> CmtDiemDuLiches { get; set; }

        public DbSet<LikeDiemDuLich> LikeDiemDuLiches { get; set; }

        public DbSet<DongGopYKien> DongGopYKiens { get; set; }

        public DbSet<ToChucDuLich> ToChucDuLiches { get; set; }
    }
}