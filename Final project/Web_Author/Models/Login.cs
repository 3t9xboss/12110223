﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class Login
    {
        [Key]
        [Display(Name = "Tài khoản")]
        [Required(ErrorMessage="Vui lòng nhập thông tin")]
        public string UserName { set; get; }
        [Display(Name = "Mật khẩu")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        public string PassWord { set; get; }
        public string Photo { set; get; }
    }
}