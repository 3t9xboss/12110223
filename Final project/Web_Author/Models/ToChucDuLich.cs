﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class ToChucDuLich
    {
        [Key]
        public int ID { set; get; }
        [Required]
        [Display(Name="Tên Địa Điểm")]
        public string TenDiaDiem { set; get; }
        [Display(Name = "Thời gian tổ chức")]
        [Required]
        [DataType(DataType.Date)]
        public DateTime ThoiGianToChuc { set; get; }
        [Display(Name = "Hình")]
        public string Hinh { set; get; }
        [Display(Name = "Nội dung thông báo")]
        [Required]
        public string NoiDung { set; get; }
        [Display(Name = "Số người đã đăng kí")]
        public int SoNguoiDaDangKy { set; get; }
        [Display(Name = "Link nhúng video")]
        public string Video { set;get; }
        [Display(Name = "Địa chỉ")]
        public string DiaChi { set; get; }
        [Display(Name = "Thông tin bổ sung")]
        public string ThongTinBoSung { set; get; }
    }
}