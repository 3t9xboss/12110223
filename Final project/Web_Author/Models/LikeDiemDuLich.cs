﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class LikeDiemDuLich
    {
        [Key]
        public int ID { set; get; }
        public int AccID { set; get; }
        
 
        public virtual Account Account { set; get; }

        public int DiemDuLichID { set; get; }
        public virtual DiemDuLich DiemDuLich { set; get; }
    }
}