﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Author.Models
{
    public class DiemDuLich
    {
        [Key]
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name="Tên Địa Điểm")]
        public string TenDiaDiem { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name="Nội Dung")]
        public string NoiDung { set; get; }
        [Display(Name="Hình")]
        public string Hinh { set; get; }
        public DateTime NgayTao { set; get; }
        public int TongLike { set; get; }
        public int TongCmt { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [Display(Name="Tên Miền(Bắc/Trung/Nam)")]
        public string Mien { set; get; }
        [Display(Name = "Đường dẫn video")]
        public string Video { set; get; }
        public virtual ICollection<CmtDiemDuLich> CmtDiemDuLich { set; get; }
        public virtual ICollection<LikeDiemDuLich> LikeDiemDLich { set; get; }
    }
}