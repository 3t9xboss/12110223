namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LikeDiemDuLiches", "Account_AccID", "dbo.Accounts");
            DropIndex("dbo.LikeDiemDuLiches", new[] { "Account_AccID" });
            RenameColumn(table: "dbo.LikeDiemDuLiches", name: "Account_AccID", newName: "AccID");
            AddForeignKey("dbo.LikeDiemDuLiches", "AccID", "dbo.Accounts", "AccID", cascadeDelete: true);
            CreateIndex("dbo.LikeDiemDuLiches", "AccID");
            DropColumn("dbo.LikeDiemDuLiches", "AccountID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LikeDiemDuLiches", "AccountID", c => c.Int(nullable: false));
            DropIndex("dbo.LikeDiemDuLiches", new[] { "AccID" });
            DropForeignKey("dbo.LikeDiemDuLiches", "AccID", "dbo.Accounts");
            RenameColumn(table: "dbo.LikeDiemDuLiches", name: "AccID", newName: "Account_AccID");
            CreateIndex("dbo.LikeDiemDuLiches", "Account_AccID");
            AddForeignKey("dbo.LikeDiemDuLiches", "Account_AccID", "dbo.Accounts", "AccID");
        }
    }
}
