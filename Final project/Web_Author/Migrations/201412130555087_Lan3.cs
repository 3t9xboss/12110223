namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CmtDiemDuLiches", "DiemDuLich_ID", "dbo.DiemDuLiches");
            DropIndex("dbo.CmtDiemDuLiches", new[] { "DiemDuLich_ID" });
            RenameColumn(table: "dbo.CmtDiemDuLiches", name: "DiemDuLich_ID", newName: "DiemDuLichID");
            AddForeignKey("dbo.CmtDiemDuLiches", "DiemDuLichID", "dbo.DiemDuLiches", "ID", cascadeDelete: true);
            CreateIndex("dbo.CmtDiemDuLiches", "DiemDuLichID");
            DropColumn("dbo.CmtDiemDuLiches", "CmtDiemDuLichID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CmtDiemDuLiches", "CmtDiemDuLichID", c => c.Int(nullable: false));
            DropIndex("dbo.CmtDiemDuLiches", new[] { "DiemDuLichID" });
            DropForeignKey("dbo.CmtDiemDuLiches", "DiemDuLichID", "dbo.DiemDuLiches");
            RenameColumn(table: "dbo.CmtDiemDuLiches", name: "DiemDuLichID", newName: "DiemDuLich_ID");
            CreateIndex("dbo.CmtDiemDuLiches", "DiemDuLich_ID");
            AddForeignKey("dbo.CmtDiemDuLiches", "DiemDuLich_ID", "dbo.DiemDuLiches", "ID");
        }
    }
}
