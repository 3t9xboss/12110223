namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToChucDuLiches", "Video", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToChucDuLiches", "Video");
        }
    }
}
