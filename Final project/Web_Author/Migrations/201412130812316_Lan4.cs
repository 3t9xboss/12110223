namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ToChucDuLiches",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenDiaDiem = c.String(nullable: false),
                        ThoiGianToChuc = c.DateTime(nullable: false),
                        Hinh = c.String(),
                        NoiDung = c.String(nullable: false),
                        SoNguoiDaDangKy = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ToChucDuLiches");
        }
    }
}
