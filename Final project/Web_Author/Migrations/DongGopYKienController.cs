﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class DongGopYKienController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /DongGopYKien/
        public ActionResult GuiThanhCong()
        {
            return View();
        }
        public ActionResult Index()
        {
            var donggopykiens = db.DongGopYKiens.Include(d => d.Account);
            return View(donggopykiens.ToList());
        }

        //
        // GET: /DongGopYKien/Details/5

        public ActionResult Details(int id = 0)
        {
            DongGopYKien donggopykien = db.DongGopYKiens.Find(id);
            if (donggopykien == null)
            {
                return HttpNotFound();
            }
            return View(donggopykien);
        }

        //
        // GET: /DongGopYKien/Create

        public ActionResult Create()
        {
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName");
            return View();
        }

        //
        // POST: /DongGopYKien/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Create(DongGopYKien donggopykien)
        {
            //string sNoiDung = collection["NoiDung"];
            //int sID = LuuTrangThai.ID;
            //donggopykien.NoiDung = "hhjjhhjbhbbhb";
            var tk = (from n in db.Accounts
                      where n.AccID == LuuTrangThai.ID
                      select n).Single();
            donggopykien.Account = tk;
            if (ModelState.IsValid)
            {
                db.DongGopYKiens.Add(donggopykien);
                db.SaveChanges();
                return RedirectToAction("GuiThanhCong","DongGopYKien");
            }

            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", donggopykien.AccID);
            return View(donggopykien);
        }

        //
        // GET: /DongGopYKien/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DongGopYKien donggopykien = db.DongGopYKiens.Find(id);
            if (donggopykien == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", donggopykien.AccID);
            return View(donggopykien);
        }

        //
        // POST: /DongGopYKien/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DongGopYKien donggopykien)
        {
            if (ModelState.IsValid)
            {
                db.Entry(donggopykien).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", donggopykien.AccID);
            return View(donggopykien);
        }

        //
        // GET: /DongGopYKien/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DongGopYKien donggopykien = db.DongGopYKiens.Find(id);
            if (donggopykien == null)
            {
                return HttpNotFound();
            }
            return View(donggopykien);
        }

        //
        // POST: /DongGopYKien/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DongGopYKien donggopykien = db.DongGopYKiens.Find(id);
            db.DongGopYKiens.Remove(donggopykien);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}