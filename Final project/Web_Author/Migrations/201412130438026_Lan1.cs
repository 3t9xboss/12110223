namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LikeDiemDuLiches", "DiemDuLich_ID", "dbo.DiemDuLiches");
            DropIndex("dbo.LikeDiemDuLiches", new[] { "DiemDuLich_ID" });
            RenameColumn(table: "dbo.LikeDiemDuLiches", name: "DiemDuLich_ID", newName: "DiemDuLichID");
            AddForeignKey("dbo.LikeDiemDuLiches", "DiemDuLichID", "dbo.DiemDuLiches", "ID", cascadeDelete: true);
            CreateIndex("dbo.LikeDiemDuLiches", "DiemDuLichID");
            DropColumn("dbo.LikeDiemDuLiches", "LikeDiemDuLichID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LikeDiemDuLiches", "LikeDiemDuLichID", c => c.Int(nullable: false));
            DropIndex("dbo.LikeDiemDuLiches", new[] { "DiemDuLichID" });
            DropForeignKey("dbo.LikeDiemDuLiches", "DiemDuLichID", "dbo.DiemDuLiches");
            RenameColumn(table: "dbo.LikeDiemDuLiches", name: "DiemDuLichID", newName: "DiemDuLich_ID");
            CreateIndex("dbo.LikeDiemDuLiches", "DiemDuLich_ID");
            AddForeignKey("dbo.LikeDiemDuLiches", "DiemDuLich_ID", "dbo.DiemDuLiches", "ID");
        }
    }
}
