namespace Web_Author.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ToChucDuLiches", "DiaChi", c => c.String());
            AddColumn("dbo.ToChucDuLiches", "ThongTinBoSung", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ToChucDuLiches", "ThongTinBoSung");
            DropColumn("dbo.ToChucDuLiches", "DiaChi");
        }
    }
}
