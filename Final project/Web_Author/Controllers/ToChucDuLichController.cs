﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class ToChucDuLichController : Controller
    {
        private WebDbContext db = new WebDbContext();

        public ActionResult ToChucDuLich()
        {
            string Chuoi3 = "";
            Chuoi3 += "Xin chào ";
            string Chuoi4 = LuuTrangThai.Name.ToString();
            ViewBag.View2 = Chuoi3;
            ViewBag.View3 = Chuoi4;
            return View();
        }
        //
        // GET: /ToChucDuLich/

        public ActionResult Index()
        {
            return View(db.ToChucDuLiches.ToList());
        }

        //
        // GET: /ToChucDuLich/Details/5

        public ActionResult Details(int id = 0)
        {
            ToChucDuLich tochucdulich = db.ToChucDuLiches.Find(id);
            if (tochucdulich == null)
            {
                return HttpNotFound();
            }
            return View(tochucdulich);
        }

        //
        // GET: /ToChucDuLich/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ToChucDuLich/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ToChucDuLich tochucdulich)
        {
            if (ModelState.IsValid)
            {
                tochucdulich.SoNguoiDaDangKy = 0;
                db.ToChucDuLiches.Add(tochucdulich);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tochucdulich);
        }

        //
        // GET: /ToChucDuLich/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ToChucDuLich tochucdulich = db.ToChucDuLiches.Find(id);
            if (tochucdulich == null)
            {
                return HttpNotFound();
            }
            return View(tochucdulich);
        }

        //
        // POST: /ToChucDuLich/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ToChucDuLich tochucdulich)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tochucdulich).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tochucdulich);
        }

        //
        // GET: /ToChucDuLich/Delete/5

        public ActionResult Delete(int id = 0)
        {
            ToChucDuLich tochucdulich = db.ToChucDuLiches.Find(id);
            if (tochucdulich == null)
            {
                return HttpNotFound();
            }
            return View(tochucdulich);
        }

        //
        // POST: /ToChucDuLich/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ToChucDuLich tochucdulich = db.ToChucDuLiches.Find(id);
            db.ToChucDuLiches.Remove(tochucdulich);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}