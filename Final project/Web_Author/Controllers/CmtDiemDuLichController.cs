﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class CmtDiemDuLichController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /CmtDiemDuLich/

        public ActionResult Index()
        {
            var cmtdiemduliches = db.CmtDiemDuLiches.Include(c => c.DiemDuLich);
            return View(cmtdiemduliches.ToList());
        }

        //
        // GET: /CmtDiemDuLich/Details/5

        public ActionResult Details(int id = 0)
        {
            CmtDiemDuLich cmtdiemdulich = db.CmtDiemDuLiches.Find(id);
            if (cmtdiemdulich == null)
            {
                return HttpNotFound();
            }
            return View(cmtdiemdulich);
        }

        //
        // GET: /CmtDiemDuLich/Create

        public ActionResult Create()
        {
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem");
            return View();
        }

        //
        // POST: /CmtDiemDuLich/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CmtDiemDuLich cmtdiemdulich,int idDiemDuLich)
        {
            var diemdulich = (from n in db.DiemDuLiches
                              where n.ID == idDiemDuLich
                              select n).Single();
            
            //cmtdiemdulich.DiemDuLich = id;
            cmtdiemdulich.AccID = LuuTrangThai.ID;
            cmtdiemdulich.Ten = LuuTrangThai.Name;
            cmtdiemdulich.DiemDuLichID = idDiemDuLich;
 
            db.CmtDiemDuLiches.Add(cmtdiemdulich);
            diemdulich.TongCmt++;
            db.Entry(diemdulich).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Details/"+idDiemDuLich,"DiemDuLich");

           // ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", cmtdiemdulich.DiemDuLichID);
           // return View(cmtdiemdulich);
        }

        //
        // GET: /CmtDiemDuLich/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CmtDiemDuLich cmtdiemdulich = db.CmtDiemDuLiches.Find(id);
            if (cmtdiemdulich == null)
            {
                return HttpNotFound();
            }
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", cmtdiemdulich.DiemDuLichID);
            return View(cmtdiemdulich);
        }

        //
        // POST: /CmtDiemDuLich/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CmtDiemDuLich cmtdiemdulich)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cmtdiemdulich).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", cmtdiemdulich.DiemDuLichID);
            return View(cmtdiemdulich);
        }

        //
        // GET: /CmtDiemDuLich/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CmtDiemDuLich cmtdiemdulich = db.CmtDiemDuLiches.Find(id);
            if (cmtdiemdulich == null)
            {
                return HttpNotFound();
            }
            return View(cmtdiemdulich);
        }

        //
        // POST: /CmtDiemDuLich/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CmtDiemDuLich cmtdiemdulich = db.CmtDiemDuLiches.Find(id);
            db.CmtDiemDuLiches.Remove(cmtdiemdulich);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}