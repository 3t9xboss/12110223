﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class TinTucController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /TinTuc/

        public ActionResult Index()
        {
            return View(db.TinTucs.ToList());
        }

        //
        // GET: /TinTuc/Details/5

        public ActionResult Details(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            return View(tintuc);
        }

        //
        // GET: /TinTuc/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /TinTuc/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TinTuc tintuc)
        {
            if (ModelState.IsValid)
            {
                tintuc.NgayTao = DateTime.Now;
                db.TinTucs.Add(tintuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tintuc);
        }

        //
        // GET: /TinTuc/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            return View(tintuc);
        }

        //
        // POST: /TinTuc/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TinTuc tintuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tintuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tintuc);
        }

        //
        // GET: /TinTuc/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            return View(tintuc);
        }

        //
        // POST: /TinTuc/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            db.TinTucs.Remove(tintuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}