﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class DiemDuLichController : Controller
    {
        LikeDiemDuLich ss = new LikeDiemDuLich();
        
        private WebDbContext db = new WebDbContext();

        //
        // GET: /DiemDuLich/
        public ActionResult DiemDuLich()
        {
           
            string Chuoi3 = "";
            Chuoi3 += "Xin chào ";
            string Chuoi4 = LuuTrangThai.Name.ToString();
            ViewBag.View2 = Chuoi3;
            ViewBag.View3 = Chuoi4;
            string Noidung = "";
            var a = from nd in db.DiemDuLiches.OrderByDescending(x => x.NgayTao)
                          select nd;
            foreach(var b in a)
            {
                
                Noidung += "<div class=\"khungdiemdulich\">";
                Noidung += "<div class=\"diemdulich-post-noidung\">";
                Noidung += "<h3 style=\"margin:0px;padding:0px\">"+b.TenDiaDiem+"</h3>";
                Noidung += "<div>";
                Noidung += "<img src=\""+b.Hinh+"\" style=\"width:400px;height:100px\"/>";
                Noidung += "</div>";
                Noidung += "<div>";
                Noidung += b.NoiDung;
                Noidung += "</div>";

                Noidung += "</div>";
                Noidung += "<div class=\"diemdulich-post-video\">";
                Noidung += "<h3 style=\"margin:0px;padding:0px\">Giới thiệu về " + b.TenDiaDiem + "</h3>";
                Noidung += "<iframe width=\"440px\" height=\"220px\" src=\"" + b.Video + "\" allowfullscreen></iframe>";
                Noidung += "</div>";

                Noidung += "<div class=\"like-cmt\">";
                Noidung += "<a href=\"http://localhost:2517/diemdulich/details/" + b.ID + "\" >";
                Noidung += "Nhấn vào đây để bình luận, đánh giá.";
                Noidung += "</a>";
                
                //Noidung += "<input id=\"Like\" style=\"float:left\" type=\"submit\" value=\"Thích\" onclick=\"BrowseServer();\" />";
                //Noidung += "<input id=\"Comment\" style=\"float:left;margin-left:50px\" type=\"button\" value=\"Bình luận\" onclick=\"BrowseServer();\" />";
                Noidung += "</div>";
                Noidung+="<input type=\"hidden\" id=\""+b.ID+"\"/>";
                Noidung += "</div>";

            }
            ViewBag.NoiDung = Noidung;
            return View();
            
        }
        public ActionResult Index()
        {
            return View(db.DiemDuLiches.ToList());
        }

        //
        // GET: /DiemDuLich/Details/5

        public ActionResult Details(int id = 0)
        {

            string Noidung = "";
            DiemDuLich diemdulich = db.DiemDuLiches.Find(id);
            int like = (from n in db.LikeDiemDuLiches
                        where (n.Account.AccID == LuuTrangThai.ID && n.DiemDuLich.ID == diemdulich.ID)
                        select n).Count(); 
            if (diemdulich == null)
            {
                return HttpNotFound();
            }
            Noidung += "<div class=\"khungdiemdulich\">";
            Noidung += "<div class=\"diemdulich-post-noidung\">";
            Noidung += "<h3 style=\"margin:0px;padding:0px\">" + diemdulich.TenDiaDiem + "</h3>";
            Noidung += "<div>";
            Noidung += "<img src=\"" + diemdulich.Hinh + "\" style=\"width:400px;height:100px\"/>";
            Noidung += "</div>";
            Noidung += "<div>";
            Noidung += diemdulich.NoiDung;
            Noidung += "</div>";

            Noidung += "</div>";
            Noidung += "<div class=\"diemdulich-post-video\">";
            Noidung += "<h3 style=\"margin:0px;padding:0px\">Giới thiệu về " + diemdulich.TenDiaDiem + "</h3>";
            Noidung += "<iframe width=\"440px\" height=\"220px\" src=\"" + diemdulich.Video + "\" allowfullscreen></iframe>";
            Noidung += "</div>";

            Noidung += "<div class=\"like-cmt\">";
            if(like==0)
                Noidung += "<input type=\"submit\" value=\"Thích\" style=\"float:left\" />";
            else
                Noidung += "<input type=\"submit\" value=\"Bỏ thích\"style=\"float:left\" />"; 
            //Noidung += "<input id=\"Comment\" style=\"float:left;margin-left:50px\" type=\"button\" value=\"Bình luận\" onclick=\"BrowseServer();\" />";
            Noidung += "</div>";
            Noidung += "<input type=\"hidden\" id=\"" + diemdulich.ID + "\"/>";
            Noidung += "</div>";
            ViewBag.NoiDung = Noidung;
            ViewData["idDiemDuLich"] = diemdulich.ID;
            return View(diemdulich);
        }

        //
        // GET: /DiemDuLich/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Details(LikeDiemDuLich like,int id=0)
        {
            
            DiemDuLich diemdulich = db.DiemDuLiches.Find(id);
            var like2 = (from n in db.LikeDiemDuLiches
                         where (n.Account.AccID == LuuTrangThai.ID && n.DiemDuLich.ID == diemdulich.ID)
                         select n);
            int like1 = like2.Count();
            if(like1==0)
            {
                diemdulich.TongLike++;
                like.Account = db.Accounts.Find(LuuTrangThai.ID);
                like.DiemDuLich = diemdulich;
                like.AccID = LuuTrangThai.ID;
                like.DiemDuLichID = diemdulich.ID;
                db.LikeDiemDuLiches.Add(like);
                db.SaveChanges();
            }
            else
            {
                diemdulich.TongLike--;
                db.LikeDiemDuLiches.Remove(like2.Single());
                db.SaveChanges();
            }
            db.Entry(diemdulich).State = EntityState.Modified;
            return Details(diemdulich.ID);
        }
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /DiemDuLich/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DiemDuLich diemdulich)
        {
            diemdulich.TongLike = 0;
            diemdulich.TongCmt = 0;
            diemdulich.NgayTao = DateTime.Now;
            diemdulich.Hinh = diemdulich.Hinh;
   
            {
                db.DiemDuLiches.Add(diemdulich);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            //return View(diemdulich);
        }

        //
        // GET: /DiemDuLich/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DiemDuLich diemdulich = db.DiemDuLiches.Find(id);
            if (diemdulich == null)
            {
                return HttpNotFound();
            }
            return View(diemdulich);
        }

        //
        // POST: /DiemDuLich/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DiemDuLich diemdulich)
        {
            if (ModelState.IsValid)
            {
                db.Entry(diemdulich).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(diemdulich);
        }

        //
        // GET: /DiemDuLich/Delete/5

        public ActionResult Delete(int id = 0)
        {
            DiemDuLich diemdulich = db.DiemDuLiches.Find(id);
            if (diemdulich == null)
            {
                return HttpNotFound();
            }
            return View(diemdulich);
        }

        //
        // POST: /DiemDuLich/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DiemDuLich diemdulich = db.DiemDuLiches.Find(id);
            db.DiemDuLiches.Remove(diemdulich);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}