﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class LoginController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Login1/
        public ActionResult Admin()
        {
            return View();
        }
        public ActionResult TrangChu()
        {
            string Chuoi = "";
            var sukien = from sk in db.SuKiens.OrderByDescending(x => x.NgayTao).Take(2)
                         select sk;
            foreach(var a in sukien)
            {
                Chuoi += " <div class=\"tintuc-image\">";
                Chuoi += "<img src=\""+a.Hinh+"\" style=\"width:100px;height:100px;margin:0px\"/>";
                Chuoi += "</div>";
                Chuoi += " <div class=\"tintuc-noidung\">";
                Chuoi += a.NoiDung;
                Chuoi += "</div>";
            }
            ViewBag.View = Chuoi;
            string Chuoi2 = "";
            var tintuc = from sk in db.TinTucs.OrderByDescending(x => x.NgayTao).Take(2)
                         select sk;
            foreach (var a in tintuc)
            {
                Chuoi2 += " <div class=\"tintuc-image\">";
                Chuoi2 += "<img src=\"" + a.Hinh + "\" style=\"width:100px;height:100px;margin:0px\"/>";
                Chuoi2 += "</div>";
                Chuoi2 += " <div class=\"tintuc-noidung\">";
                Chuoi2 += a.NoiDung;
                Chuoi2 += "</div>";
            }
            string Chuoi3 = "";
            Chuoi3 += "Xin chào ";
            string Chuoi4 = LuuTrangThai.Name.ToString();
            ViewBag.View2 = Chuoi3;
            ViewBag.View3 = Chuoi4;
            ViewBag.View1 = Chuoi2;
            return View();
        }
        public ActionResult ChuyenTrang()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View(db.Logins.ToList());
        }

        //
        // GET: /Login1/Details/5

        public ActionResult Details(string id = null)
        {
            Login login = db.Logins.Find(id);
            if (login == null)
            {
                return HttpNotFound();
            }
            return View(login);
        }

        //
        // GET: /Login1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Login1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Login login)
        {
            bool check=false;
            var acc = from n in db.Accounts
                      where n.UserName==login.UserName
                      select n;
            foreach(var n in acc)
            {
                if (login.UserName == n.UserName && login.PassWord == n.Password) check = true;
            }
            if (check==true)
            {
                //db.Logins.Add(login);
                //db.SaveChanges();
                LuuTrangThai.ID = acc.Single().AccID;
                LuuTrangThai.UserName = acc.Single().UserName;
                LuuTrangThai.Name = acc.Single().Name;
                return RedirectToAction("TrangChu","Login");
            }
            else
            {
                ModelState.AddModelError("", "Tài khoản hoặc mật khẩu không đúng.");
                return View(login);
            }
            
        }

        //
        // GET: /Login1/Edit/5

        public ActionResult Edit()
        {

            return View();
        }

        //
        // POST: /Login1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ChangePassword account,Account accountX)
        {
            var acc = from n in db.Accounts
                      select n;
            int b = 0;
            foreach(var a in acc)
            {
                if (a.UserName == account.UserName)
                    if (a.Email == account.Email)
                        if (a.DayOfBird == account.DayOfBird)
                            if (account.NewPassword == account.Confirm)
                                b = 4;
                            else
                                b = 3;
                        else
                            b = 2;
                    else
                        b = 1;

            }
            if(b==4)
            {
                accountX = db.Accounts.Single(c => c.UserName == account.UserName);
                accountX.Password = account.NewPassword;
                db.Entry(accountX).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ChuyenTrang", "login");
            }
            return View();
        }

        //
        // GET: /Login1/Delete/5

        public ActionResult Delete(string id = null)
        {
            Login login = db.Logins.Find(id);
            if (login == null)
            {
                return HttpNotFound();
            }
            return View(login);
        }

        //
        // POST: /Login1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            Login login = db.Logins.Find(id);
            db.Logins.Remove(login);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}