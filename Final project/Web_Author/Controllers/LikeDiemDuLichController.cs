﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Author.Models;

namespace Web_Author.Controllers
{
    public class LikeDiemDuLichController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /LikeDiemDuLich/

        public ActionResult Index()
        {
            var likediemduliches = db.LikeDiemDuLiches.Include(l => l.Account).Include(l => l.DiemDuLich);
            return View(likediemduliches.ToList());
        }

        //
        // GET: /LikeDiemDuLich/Details/5

        public ActionResult Details(int id = 0)
        {
            LikeDiemDuLich likediemdulich = db.LikeDiemDuLiches.Find(id);
            if (likediemdulich == null)
            {
                return HttpNotFound();
            }
            return View(likediemdulich);
        }

        //
        // GET: /LikeDiemDuLich/Create

        public ActionResult Create()
        {
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName");
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem");
            return View();
        }

        //
        // POST: /LikeDiemDuLich/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LikeDiemDuLich likediemdulich)
        {
            if (ModelState.IsValid)
            {
                db.LikeDiemDuLiches.Add(likediemdulich);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", likediemdulich.AccID);
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", likediemdulich.DiemDuLichID);
            return View(likediemdulich);
        }

        //
        // GET: /LikeDiemDuLich/Edit/5

        public ActionResult Edit(int id = 0)
        {
            LikeDiemDuLich likediemdulich = db.LikeDiemDuLiches.Find(id);
            if (likediemdulich == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", likediemdulich.AccID);
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", likediemdulich.DiemDuLichID);
            return View(likediemdulich);
        }

        //
        // POST: /LikeDiemDuLich/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LikeDiemDuLich likediemdulich)
        {
            if (ModelState.IsValid)
            {
                db.Entry(likediemdulich).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccID = new SelectList(db.Accounts, "AccID", "UserName", likediemdulich.AccID);
            ViewBag.DiemDuLichID = new SelectList(db.DiemDuLiches, "ID", "TenDiaDiem", likediemdulich.DiemDuLichID);
            return View(likediemdulich);
        }

        //
        // GET: /LikeDiemDuLich/Delete/5

        public ActionResult Delete(int id = 0)
        {
            LikeDiemDuLich likediemdulich = db.LikeDiemDuLiches.Find(id);
            if (likediemdulich == null)
            {
                return HttpNotFound();
            }
            return View(likediemdulich);
        }

        //
        // POST: /LikeDiemDuLich/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LikeDiemDuLich likediemdulich = db.LikeDiemDuLiches.Find(id);
            db.LikeDiemDuLiches.Remove(likediemdulich);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}