﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Tag
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự từ 10 đến 100", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}