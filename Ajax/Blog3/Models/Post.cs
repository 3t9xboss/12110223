﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog3.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20 đến 500", MinimumLength = 20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdate { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}