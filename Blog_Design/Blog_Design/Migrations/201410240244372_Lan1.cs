namespace Blog_Design.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Accounts", "Firstname", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Accounts", "Lastname", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Lastname", c => c.String(nullable: false));
            AlterColumn("dbo.Accounts", "Firstname", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
        }
    }
}
