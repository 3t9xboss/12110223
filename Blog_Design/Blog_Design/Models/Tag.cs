﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Design.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [StringLength(100,ErrorMessage="Kí tự nằm trong khoảng 10-100",MinimumLength=10)]
        public string Content { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}