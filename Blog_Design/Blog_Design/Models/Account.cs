﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Design.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required(ErrorMessage="Vui lòng nhập thông tin")]
        [DataType(DataType.Password)]
        public string Password { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Định dạng email dạng: abc@example.com")]
        public string Email { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [StringLength(100, ErrorMessage = "Tối đa 100 kí tự")]
        public string Firstname { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [StringLength(100, ErrorMessage = "Tối đa 100 kí tự")]
        public string Lastname { set; get; }
        public virtual ICollection<Post> Posts { set; get; }
    }
}