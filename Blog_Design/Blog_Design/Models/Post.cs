﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Design.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [StringLength(500,ErrorMessage="20-500 kí tự",MinimumLength=20)]
        public string Title { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Tối thiểu 50 kí tự")]
        public string Body { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [RegularExpression(@"^(1[0-2]|0[1-9])[-/](3[01]|[12][0-9]|0[1-9])[-/][0-9]{4}$", ErrorMessage = "Ngày tháng có dạng mm/dd/yyyy")]
        [DataType(DataType.DateTime)]
        public DateTime DayCreated { set; get; }
        [Required(ErrorMessage = "Vui lòng nhập thông tin")]
        [DataType(DataType.DateTime)]
        [RegularExpression(@"^(1[0-2]|0[1-9])[-/](3[01]|[12][0-9]|0[1-9])[-/][0-9]{4}$", ErrorMessage = "Ngày tháng có dạng mm/dd/yyyy")]
        public DateTime DayUpdated { set; get; }
        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}